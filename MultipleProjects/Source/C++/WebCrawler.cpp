#include "../Headers/UtilityFunctions.h"
#include "../Headers/WebCrawler.h"

void WebCrawler::GetInitialUrl()
{
	cout << inputPrompt << endl;
	string myUrl;
	cin >> myUrl;

	if (UtilityFunctions::ToLower(myUrl) != "")
	{
		urlQueue.push(myUrl);
	}
}

SOCKET WebCrawler::Connect(string serverName, WORD portNum)
{
	const char *charServerName = serverName.c_str();

	// Host entity structure, used to keep host related data
	struct hostent *hp;
	unsigned int addr;

	// SockAddr_in structure specifies transport address and port for AF_INET (IPv4) address family 
	struct sockaddr_in server;
	SOCKET conn;

	// create socket connection
	conn = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);

	if (conn == INVALID_SOCKET)
	{
		return NULL;
	}

	// inet_addr converts IPv4 address to binary data in network byte order
	if (inet_addr(charServerName) == INADDR_NONE)
	{
		hp = gethostbyname(charServerName);
	}
	else
	{
		addr = inet_addr(charServerName);

		// we use reference of addr to not change the actual addr, but to simply use its value
		hp = gethostbyaddr((char*)&addr, sizeof(addr), AF_INET);
	}

	if (hp == NULL)
	{
		closesocket(conn);
		return NULL;
	}
	
	server.sin_addr.s_addr = *((unsigned long*)hp->h_addr);
	server.sin_family = AF_INET;
	server.sin_port = htons(portNum);

	// connect function returns 0 when connection is possible, which is actually false when used as a bool,
	// this means the below will be "false" (0) when connection is made
	// any other value returned by connect will be an error code
	if (connect(conn, (struct sockaddr*)&server, sizeof(server)))
	{
		closesocket(conn);
		return NULL;
	}

	return conn;
}

void WebCrawler::ParseUrl(string url, string serverName, string filePath, string fileName)
{
	// size_type == maximum size that a string can be
	string::size_type subUrlIndex;
	
	if (url.substr(0, 7) == "http://")
	{
		url.erase(0, 7);
	}

	if (url.substr(0, 8) == "https://")
	{
		url.erase(0, 8);
	}

	subUrlIndex = url.find('/');

	// npos == -1 on default, we use npos instead of -1 for better legibility
	if (subUrlIndex != string::npos)
	{
		// if '/' is found
		serverName = url.substr(0, subUrlIndex);
		filePath = url.substr(subUrlIndex);
		// rfind == find from right side
		subUrlIndex = filePath.rfind('/');
		fileName = filePath.substr(subUrlIndex + 1);
	}

	else
	{
		serverName = url;
		filePath = "/";
		fileName = "";
	}
}

int WebCrawler::GetHeaderLength(string content)
{
	const char* charContent = content.c_str();
	const char* searchString1 = "\r\n\r\n";
	const char* searchString2 = "\n\r\n\r";
	const char* findPosition;
	int offset = -1;

	findPosition = strstr(charContent, searchString1);

	if (findPosition != NULL)
	{
		offset = findPosition - charContent;
		offset += strlen(searchString1);
	}

	else
	{
		findPosition = strstr(charContent, searchString2);
		if (findPosition != NULL)
		{
			offset = findPosition - charContent;
			offset += strlen(searchString2);
		}
	}

	return offset;
}

string WebCrawler::ReadUrl(string url, long bytesReturnedOut, string headerOut)
{
	const int bufferSize = 512;
	char readBuffer[bufferSize];
	char sendBuffer[bufferSize];
	char tempBuffer[bufferSize];

	char *tempResult = NULL, *result;

	SOCKET connection;

	string server, filePath, fileName;

	long totalBytesRead, thisReadSize, headerLength;

	ParseUrl(url, server, filePath, fileName);

	connection = Connect(server, 80);

	sprintf(tempBuffer, "GET %s HTTP/1.0", filePath);
	strcpy(sendBuffer, tempBuffer);
	strcat(sendBuffer, "\r\n");
	sprintf(tempBuffer, "Host: %s", server);
	strcat(sendBuffer, tempBuffer);
	strcat(sendBuffer, "\r\n");
	strcat(sendBuffer, "\r\n");
	send(connection, sendBuffer, strlen(sendBuffer), 0);

	printf("Buffer being sent:\n%s", sendBuffer);

	totalBytesRead = 0;

	while (1)
	{
		memset(readBuffer, 0, bufferSize);
		// recv - receive data from connected socket
		thisReadSize = recv(connection, readBuffer, bufferSize, 0);

		if (thisReadSize <= 0)
		{
			break;
		}

		tempResult = (char*)realloc(tempResult, thisReadSize + totalBytesRead);

		memcpy(tempResult + totalBytesRead, readBuffer, thisReadSize);
		totalBytesRead += thisReadSize;
	}

	headerLength = GetHeaderLength(std::string (tempResult));

	long contentLength = totalBytesRead - headerLength;
	result = new char[contentLength + 1];

	memcpy(result, tempResult + headerLength, contentLength);
	result[contentLength] = 0x0;
	char *myTemp;

	myTemp = new char[headerLength + 1];
	strncpy(myTemp, tempResult, headerLength);
	myTemp[headerLength] = NULL;
	delete(tempResult);
	string headerOut = std::string (myTemp);

	bytesReturnedOut = contentLength;
	closesocket(connection);
	return(result);
}