#include "../Headers/UtilityFunctions.h"

string UtilityFunctions::ToLower(string myInput)
{
	std::for_each(myInput.begin(), myInput.end(), CharToLower);
	return myInput;
}

void UtilityFunctions::CharToLower(char& c)
{
	c = std::tolower(static_cast<unsigned char>(c));
}

int UtilityFunctions::CharToInt(char& c)
{
	return c - '0';
}


bool UtilityFunctions::IsNumericalString(string myInput)
{
	try
	{
		atoi(myInput.c_str());
	}
	catch (exception& e)
	{
		return false;
	}
	return true;
}
