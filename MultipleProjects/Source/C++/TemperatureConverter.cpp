#include "../Headers/TemperatureConverter.h"

using namespace std;

string TemperatureConverter::GetUserInput()
{
	cout << inputPrompt << endl;
	string myInput;
	cin >> myInput;

	if (UtilityFunctions::ToLower(myInput) != "q")
	{
		string numericalValue, temperature;
		
		for (int i = 0; i < myInput.length(); i++)
		{
			char c = myInput[i];

			if (isdigit(c) || c == '.' || (c == '-' && i == 0))
			{
				numericalValue.push_back(c);
			}
			else
			{
				temperature.push_back(c);
			}
		}

		if (UtilityFunctions::ToLower(temperature) == "c" || UtilityFunctions::ToLower(temperature) == "f" || UtilityFunctions::ToLower(temperature) == "k")
		{
			if (UtilityFunctions::ToLower(temperature) == "c")
			{
				currentType = TemperatureType::Celcius;
			}
			else if (UtilityFunctions::ToLower(temperature) == "f")
			{
				currentType = TemperatureType::Fahrenheit;
			}
			else
			{
				currentType = TemperatureType::Kelvin;
			}

			return numericalValue;
		}
		else
		{
			return GetUserInput();
		}
	}
	return myInput;
}

void TemperatureConverter::DoConversion()
{
	string userInput = GetUserInput();
	while (UtilityFunctions::ToLower(userInput) != "q")
	{
		GetConversion(userInput);
		userInput = GetUserInput();
	}
}

bool TemperatureConverter::GetConversion(string myInput)
{
	float flatValue;
	try
	{
		flatValue = stof(myInput);
	} catch (exception& e)
	{
		return false;
	}

	double celcius;
	double fahrenheit;
	double kelvin;

	switch (currentType)
	{
		case TemperatureType::Celcius:
			celcius = flatValue;
			fahrenheit = ((celcius * 9) / 5) + 32;
			kelvin = celcius + 273.15;
			break;

		case TemperatureType::Fahrenheit:
			fahrenheit = flatValue;
			celcius = ((fahrenheit - 32) * 5) / 9;
			kelvin = celcius + 273.15;
			break;

		case TemperatureType::Kelvin:
			kelvin = flatValue;
			celcius = kelvin - 273.15;
			fahrenheit = ((celcius * 9) / 5) + 32;
			break;	
	}

	cout << "Celcius: " << celcius << " | Fahrenheit " << fahrenheit << " | Kelvin " << kelvin << endl;
	cout << endl;
	return true;
}

