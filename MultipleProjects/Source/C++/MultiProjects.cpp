// MultiProjects.cpp : Defines the entry point for the console application.
//

#include "..\Headers\MultiProjects.h"

using namespace std;

int MultiProjects::MainMenu()
{
	cout << "Hello! Please choose a mode of operation: " << endl;
	cout << "	- " + projects[Projects::DecimalBinary] + " (1): " << endl;
	cout << "	- " + projects[Projects::TemperatureConverter] + " (2): " << endl;
	cout << "	- " + projects[Projects::VowelCounter] + " (3): " << endl;
	cout << "	- " + projects[Projects::IsPalindrome] + " (4): " << endl;
	cout << "	- " + projects[Projects::WebCrawler] + " (5): "<< endl;
	cout << "	- " + projects[Projects::PdfGenerator] + " (6): " << endl;
	cout << "	- " + projects[Projects::TextEditor] + " (7): " << endl;
	cout << "	- " + projects[Projects::JiraTicketUploader] + " (8): " << endl;
	cout << "	- " + projects[Projects::Exit] + " (9): " << endl;

	string input = "";
	cin >> input;
	cout << endl;

	try
	{
		return atoi(input.c_str()) - 1;
	}
	catch (const exception& e)
	{
		cout << errorMessage << endl;
		return -1;
	}
}

string MultiProjects::GetErrorMessage()
{
	return errorMessage;
}

int MultiProjects::ProjectsCount()
{
	return projects.size();
}

int main()
{
	MultiProjects projects;
	DecimalBinaryConverter decBinConverter;
	TemperatureConverter tempConverter;
	VowelCounter vowelCounter;
	PalindromeChecker palindromeChecker;

	int modeSelection;
	
	do
	{
		modeSelection = projects.MainMenu();

		switch (modeSelection)
		{
		case (int)MultiProjects::Projects::DecimalBinary:
			decBinConverter.DoConversion();
			break;
		case (int)MultiProjects::Projects::TemperatureConverter:
			tempConverter.DoConversion();
			break;
		case (int)MultiProjects::Projects::VowelCounter:
			vowelCounter.DoCount();
			break;
		case (int)MultiProjects::Projects::IsPalindrome:
			palindromeChecker.DoPalindromeCheck();
			break;
		case (int)MultiProjects::Projects::WebCrawler:
			break;
		case (int)MultiProjects::Projects::PdfGenerator:
			break;
		case (int)MultiProjects::Projects::TextEditor:
			break;
		case (int)MultiProjects::Projects::JiraTicketUploader:
			break;
		case (int)MultiProjects::Projects::Exit:
			return 0;
		default:
			cout << projects.GetErrorMessage() << endl;
			break;
		}
	} while (modeSelection < 0 || modeSelection != MultiProjects::Projects::Exit);

    return 0;
}

