#include "../Headers/VowelCounter.h"

using namespace std;

string VowelCounter::GetUserInput()
{
	string userInput;
	cout << inputPrompt << endl;
	cin >> userInput;

	return userInput;
}

void VowelCounter::DoCount()
{
	string userInput = GetUserInput();
	while (UtilityFunctions::ToLower(userInput) != "")
	{
		bool intInput = false;
		try
		{
			stoi(userInput);
			intInput = true;
		}
		catch (exception invalid_argument)
		{
			GetCount(userInput);
			userInput = GetUserInput();
		}

		if (intInput)
		{
			break;
		}
	}
}

void VowelCounter::GetCount(string input)
{
	int vowelCount = 0;
	int stringLength = input.length();

	for (int i = 0; i < stringLength; i++)
	{
		string currChar = { input[i] };
		size_t found = vowels.find(currChar);

		if (found != string::npos)
		{
			vowelCount++;
		}
	}

	cout << vowelCount << endl;
}