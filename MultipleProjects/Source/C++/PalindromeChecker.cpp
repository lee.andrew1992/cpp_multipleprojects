#include "../Headers/PalindromeChecker.h"

using namespace std;

string PalindromeChecker::GetUserInput()
{
	string userInput;
	getline(cin, userInput);

	return userInput;
}

void PalindromeChecker::DoPalindromeCheck()
{
	PrintPrompt();
	string userInput = GetUserInput();

	do 
	{
		if (!userInput.empty())
		{
			IsPalindrome(userInput);
			PrintPrompt();
		}
		userInput = GetUserInput();

	} while (UtilityFunctions::ToLower(userInput) != "");
}

void PalindromeChecker::IsPalindrome(string inputString)
{
	int stringSize = inputString.length();
	bool isSameCharacter = false;
	
	for (int index = 0; index < stringSize; index++)
	{
		isSameCharacter = SameCharacter(inputString[index], inputString[stringSize - (index + 1)]);
		if (!isSameCharacter)
		{
			break;
		}
	}

	cout << "<" << inputString << "> is palindrome? " << boolalpha << (bool)isSameCharacter << endl;
}

void PalindromeChecker::PrintPrompt()
{
	cout << inputPrompt << endl;
}

bool PalindromeChecker::SameCharacter(char char1, char char2)
{
	return char1 == char2;
}