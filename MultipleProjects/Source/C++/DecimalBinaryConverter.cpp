#include "../Headers/DecimalBinaryConverter.h"

using namespace std;

string DecimalBinaryConverter::GetUserInput()
{
	cout << inputPrompt << endl;
	string input;
	cin >> input;

	if (UtilityFunctions::ToLower(input) != "q")
	{
		string binaryCheck = "01";
		for (char& c : input)
		{
			if (binaryCheck.find(c) == -1)
			{
				mode = InputType::Integer;

				return input;
			}
		}

		if (mode != InputType::Integer)
		{
			cout << intTypePrompt << endl;
			string intType;
			cin >> intType;
			UtilityFunctions::ToLower(intType);

			while (intType != "b" && intType != "i")
			{
				cout << intType << endl;
				cout << intTypePrompt << endl;
				string intType;
			}

			if (intType == "b")
				mode = InputType::Binary;
			else if (intType == "i")
				mode = InputType::Integer;
		}
	}
	return input;
}

bool DecimalBinaryConverter::DoConversion()
{
	string myInput = GetUserInput();
	while (myInput != "q")
	{
		GetConversion(myInput);
		mode = InputType::None;
		myInput = GetUserInput();
	}

	return true;
}

bool DecimalBinaryConverter::GetConversion(string myInput)
{
	try
	{
		if (mode == InputType::Binary)
		{
			cout << stol(myInput, nullptr, 2) << endl;
		}
		else if (mode == InputType::Integer)
		{
			long intValue = stol(myInput);
			string binValue = "";
			while (intValue > 1)
			{
				binValue += to_string(intValue % 2);
				intValue = intValue / 2;
			}

			binValue += to_string(1);

			reverse(binValue.begin(), binValue.end());

			cout << binValue << endl;
			cout << endl;
		}
	}
	catch (const exception& e)
	{
		cout << endl;
		return false;
	}

	return true;
}
