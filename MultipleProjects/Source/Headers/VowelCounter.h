#include <string>
#include <iostream>
#include "../Headers/UtilityFunctions.h"

using namespace std;

class VowelCounter
{
public:
	VowelCounter() {}

	string GetUserInput();
	void DoCount();

private:
	string inputPrompt = "Please enter a string (Enter numerical value to quit): ";
	string vowels = "aeiou";

	void GetCount(string input);
};