#pragma once
#include <string>
#include <unordered_map>
#include "DecimalBinaryConverter.h"
#include "TemperatureConverter.h"
#include "VowelCounter.h"
#include "PalindromeChecker.h"
#include <iostream>
#include <cstdio>


using namespace std;

class MultiProjects
{
	public:
		enum Projects
		{
			DecimalBinary = 0,
			TemperatureConverter = 1,
			VowelCounter = 2,
			IsPalindrome = 3,
			WebCrawler = 4,
			PdfGenerator = 5,
			TextEditor = 6,
			JiraTicketUploader = 7,
			Exit = 8
		};

		MultiProjects()
		{
			errorMessage = "Please Enter a valid mode of operation (1~9)";
			projects.insert(pair<Projects, string>(Projects::DecimalBinary, "Decimal <-> Binary Converter"));
			projects.insert(pair<Projects, string>(Projects::TemperatureConverter, "Temperature Converter"));
			projects.insert(pair<Projects, string>(Projects::VowelCounter, "Vowel Counter"));
			projects.insert(pair<Projects, string>(Projects::IsPalindrome, "Palindrome Checker"));
			projects.insert(pair<Projects, string>(Projects::WebCrawler, "Web Crawler"));
			projects.insert(pair<Projects, string>(Projects::PdfGenerator, "PDF Generator"));
			projects.insert(pair<Projects, string>(Projects::TextEditor, "Text Editor"));
			projects.insert(pair<Projects, string>(Projects::JiraTicketUploader, "JIRA Ticket Uploader"));
			projects.insert(pair<Projects, string>(Projects::Exit, "Quit Program"));
		}
		int ProjectsCount();
		int MainMenu();
		string GetErrorMessage();

private:
		string errorMessage;
		unordered_map<Projects, string> projects;
};
