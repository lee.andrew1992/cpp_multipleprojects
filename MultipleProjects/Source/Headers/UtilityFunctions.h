#pragma once

#include <string>
#include <algorithm>
#include <cctype>

using namespace std;

class UtilityFunctions
{
public:
	static string ToLower(string myInput);
	static int CharToInt(char& c);
	static bool IsNumericalString(string myInput);

private:
	UtilityFunctions() {}

	static void CharToLower(char& c);
};