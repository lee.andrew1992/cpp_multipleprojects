#include <string>
#include <iostream>
#include "../Headers/UtilityFunctions.h"

using namespace std;

class PalindromeChecker
{
public:
	PalindromeChecker() {}

	string GetUserInput();
	void DoPalindromeCheck();

private:
	string inputPrompt = "[Palindrome Checker] Please enter a string (Enter empty value to quit): ";
	void IsPalindrome(string inputString);
	bool SameCharacter(char char1, char char2);
	void PrintPrompt();
};