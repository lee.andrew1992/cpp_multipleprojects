#include <string>
#include <iostream>
#include <queue>
#include <windows.h>
#include "UtilityFunctions.h"

using namespace std;

class WebCrawler
{
public:
	WebCrawler() {}

	void GetInitialUrl();

private:
	string inputPrompt = "Enter URL Address (Enter empty string to quit):";
	queue<string> urlQueue;

	HINSTANCE hInst;
	WSADATA wsaData;

	SOCKET Connect(string serverName, WORD portNum);
	void ParseUrl(string url, string serverName, string filePath, string fileName);
	int GetHeaderLength(string content);
	string ReadUrl(string url, long bytesReturnedOut, string headerOut);
};