#include <string>
#include "UtilityFunctions.h"
#include <iostream>
#include <algorithm>
#include <math.h>

using namespace std;

class DecimalBinaryConverter
{
public:
	DecimalBinaryConverter()
	{
	}

	string GetUserInput();
	bool DoConversion();

private:
	bool GetConversion(string myInput);
	enum InputType
	{
		None = -1,
		Integer = 0,
		Binary = 1
	};

	string inputPrompt = "[Decimal Binary Converter] Please enter an integer or binary number <input 'q' to quit>: ";
	string intTypePrompt = "Is your number a binary or integer? (b/i): ";
	int mode = InputType::None;
};
