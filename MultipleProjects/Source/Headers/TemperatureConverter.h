#include <string>
#include <iostream>
#include "../Headers/UtilityFunctions.h"

using namespace std;

class TemperatureConverter
{
public:
	TemperatureConverter()
	{
	}

	string GetUserInput();
	void DoConversion();

	enum TemperatureType
	{
		Kelvin,
		Celcius,
		Fahrenheit
	};

	TemperatureType currentType;

private:
	bool GetConversion(string myInput);
	string inputPrompt = "[Temp Converter] Please enter a temperature followed by a temperature unit (ex. 24.5C) <input 'q' to quit>: ";
	string errorPrompt = "Please enter a proper temperature value (ex. 24.5C)";
};